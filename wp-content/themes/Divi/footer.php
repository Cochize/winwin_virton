	<footer id="main-footer">
		<?php get_sidebar( 'footer' ); ?>
		<div class="et_pb_row" id="partenaires">
			<img src="http://localhost/winwin_virton/wp-content/uploads/2015/10/virton.jpg" />
			<img src="http://localhost/winwin_virton/wp-content/uploads/2015/10/service_provincial.jpg" />
			<img src="http://localhost/winwin_virton/wp-content/uploads/2015/10/province.jpg" />
			<img src="http://localhost/winwin_virton/wp-content/uploads/2015/10/cohesion.jpg" />
			<img src="http://localhost/winwin_virton/wp-content/uploads/2015/10/sivirton.jpg" />
			<img src="http://localhost/winwin_virton/wp-content/uploads/2015/10/bnp_paribas_fortis.jpg" />
			<img src="http://localhost/winwin_virton/wp-content/uploads/2015/10/foundation_bnp_paribas_fortis.jpg" />
			<img src="http://localhost/winwin_virton/wp-content/uploads/2015/10/ages.jpg" />
			<img src="http://localhost/winwin_virton/wp-content/uploads/2015/10/cpas.jpg" />
			<img src="http://localhost/winwin_virton/wp-content/uploads/2015/10/foot.jpg" />
			<img src="http://localhost/winwin_virton/wp-content/uploads/2015/10/HAPCF.jpg" />
			<img src="http://localhost/winwin_virton/wp-content/uploads/2015/10/lions.jpg" />
			<img src="http://localhost/winwin_virton/wp-content/uploads/2015/10/maisontourisme.jpg" />
			<img src="http://localhost/winwin_virton/wp-content/uploads/2015/10/mjvirton.jpg" />
			<img src="http://localhost/winwin_virton/wp-content/uploads/2015/10/aca.jpg" />

			
			
		</div>
		<div id="footer-bottom">
			<div class="container clearfix">
				<ul id="et-social-icons">
				<?php if ( 'on' === et_get_option( 'divi_show_facebook_icon', 'on' ) ) : ?>
					<li class="et-social-icon et-social-facebook">
						<a href="<?php echo esc_url( et_get_option( 'divi_facebook_url', '#' ) ); ?>">
							<span><?php esc_html_e( 'Facebook', 'Divi' ); ?></span>
						</a>
					</li>
				<?php endif; ?>
				<?php if ( 'on' === et_get_option( 'divi_show_twitter_icon', 'on' ) ) : ?>
					<li class="et-social-icon et-social-twitter">
						<a href="<?php echo esc_url( et_get_option( 'divi_twitter_url', '#' ) ); ?>">
							<span><?php esc_html_e( 'Twitter', 'Divi' ); ?></span>
						</a>
					</li>
				<?php endif; ?>
				<?php if ( 'on' === et_get_option( 'divi_show_google_icon', 'on' ) ) : ?>
					<li class="et-social-icon et-social-google">
						<a href="<?php echo esc_url( et_get_option( 'divi_google_url', '#' ) ); ?>">
							<span><?php esc_html_e( 'Google', 'Divi' ); ?></span>
						</a>
					</li>
				<?php endif; ?>
				<?php if ( 'on' === et_get_option( 'divi_show_rss_icon', 'on' ) ) : ?>
				<?php
					$et_rss_url = '' !== et_get_option( 'divi_rss_url' )
						? et_get_option( 'divi_rss_url' )
						: get_bloginfo( 'comments_rss2_url' );
				?>
					<li class="et-social-icon et-social-rss">
						<a href="<?php echo esc_url( $et_rss_url ); ?>">
							<span><?php esc_html_e( 'RSS', 'Divi' ); ?></span>
						</a>
					</li>
				<?php endif; ?>
				</ul>

				<p id="footer-info"><?php echo '<a href="http://www.winwin.be" title="Winwin Bourses d&rsquo;&eacute;changes">Winwin | Bourses d&rsquo;&eacute;changes</a> &copy Tous droits r&eacute;serv&eacute;s'; ?></p>
			</div>	<!-- .container -->
		</div>
	</footer> <!-- #main-footer -->

	<?php wp_footer(); ?>
</body>
</html>