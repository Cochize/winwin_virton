=== Simple Analytics ===
Author URI: http://www.jasonbobich.com
Contributors: themeblvd
Tags: google, analytics, tracking, Theme Blvd, themeblvd, Jason Bobich
Stable Tag: 1.0.0

A simple plugin to include your Google Analtyics tracking.

== Description ==

This plugin allows you to quickly include your Google Analytics tracking. After installing the plugin, just go to *Settings > Analytics* and input your Google Analytics Tracking ID.

== Installation ==

1. Upload `simple-analytics` folder to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Go to *Settings > Analytics* to configure.

*Note: Tracking code will not be inserted for the logged-in admin user.*

== Screenshots ==

1. The settings page at Settings > Analytics.

== Changelog ==

= 1.0.0 =

* This is the first release.